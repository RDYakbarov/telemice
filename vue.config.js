const path = require("path");

module.exports = {
  css: {
    loaderOptions: {
      scss: {
        data: `@import "~@/styles/settings/colors.scss"; @import "~@/styles/settings/variables.scss";`
      }
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@assets", path.resolve("", "src/assets"));
    config.resolve.alias.set("@api", path.resolve("", "api/"));
  }
};
