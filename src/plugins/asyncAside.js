import Vue from "vue";
//TODO переделать на автосбор компонентов
const EventsCreate = () => ({
  component: import("@/components/The/Aside/Events/Create")
});
const LandingLogin = () => ({
  component: import("@/components/The/Aside/Landing/Login")
});
const LandingRecovery = () => ({
  component: import("@/components/The/Aside/Landing/Recovery")
});
const LandingRegistration = () => ({
  component: import("@/components/The/Aside/Landing/Registration")
});

const LandingPassword = () => ({
  component: import("@/components/The/Aside/Landing/Password")
});

Vue.component("EventsCreate", EventsCreate);
Vue.component("LandingLogin", LandingLogin);
Vue.component("LandingPassword", LandingPassword);
Vue.component("LandingRecovery", LandingRecovery);
Vue.component("LandingRegistration", LandingRegistration);
