import Vue from "vue";
import {
  Button,
  Checkbox,
  Radio,
  Notification,
  Input,
  Select,
  Option,
  Avatar,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  Drawer,
  Form,
  FormItem,
  Row,
  Col,
  InputNumber,
  Container,
  Aside,
  Menu,
  Submenu,
  MenuItem,
  MenuItemGroup,
  RadioButton,
  RadioGroup
} from "element-ui";
import lang from "element-ui/lib/locale/lang/ru-RU";
import locale from "element-ui/lib/locale";

locale.use(lang);

Vue.component("base-button", Button);
Vue.component("base-checkbox", Checkbox);
Vue.component("base-radio", Radio);
Vue.component("base-input", Input);
Vue.component("base-input-number", InputNumber);
Vue.component("base-select", Select);
Vue.component("base-option", Option);
Vue.component("base-ava", Avatar);
Vue.component("base-dropdown", Dropdown);
Vue.component("base-dropdown-item", DropdownItem);
Vue.component("base-dropdown-menu", DropdownMenu);
Vue.component("base-drawer", Drawer);
Vue.component("base-form", Form);
Vue.component("base-form-item", FormItem);
Vue.component("base-row", Row);
Vue.component("base-col", Col);
Vue.component("base-container", Container);
Vue.component("base-aside", Aside);
Vue.component("base-menu", Menu);
Vue.component("base-submenu", Submenu);
Vue.component("base-menu-item", MenuItem);
Vue.component("base-menu-item-group", MenuItemGroup);
Vue.component("base-radio-group", RadioGroup);
Vue.component("base-radio-button", RadioButton);

Vue.prototype.$notify = Notification;
