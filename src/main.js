import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import "./plugins/icons.js";
import "./plugins/element.js";
import "./plugins/scrollTo.js";
import "./plugins/asyncAside.js";
import "@/styles/overrides/index.scss";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
