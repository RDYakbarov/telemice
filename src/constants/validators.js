export const email = {
  required: true,
  validator(rule, value, callback) {
    if (!value) {
      return callback(new Error("Поле обязательно для заполнения"));
    }
    return /.+@\w{2,}\.\w{2,}/.test(value)
      ? callback()
      : callback(new Error("Введите корректный email"));
  }
};

export const password = {
  required: true,
  min: 8,
  trigger: "blur",
  message: "Пароль должен быть не менее 8 символов"
};

export const commonValidator = {
  email,
  password
};
