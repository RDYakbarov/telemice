import Vue from "vue";
import Router from "vue-router";
import { getAuthCookie } from "@/utils/common";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/events",
      component: () =>
        import(/* webpackChunkName: "events" */ "./views/Events/"),
      meta: {
        needAuth: true
      },
      children: [
        {
          path: "",
          name: "events",
          component: () =>
            import(
              /* webpackChunkName: "events-index" */ "./views/Events/Index/"
            )
        },
        {
          path: "info",
          component: () =>
            import(
              /* webpackChunkName: "events-info" */ "./views/Events/Info/"
            ),
          meta: {
            layout: "ServiceInfoNavigation"
          }
        }
      ]
    },
    {
      path: "/",
      name: "main",
      component: () =>
        import(/* webpackChunkName: "landing" */ "./views/Landing"),
      meta: {
        needAuth: false
      }
    }
  ]
});

router.beforeEach(({ meta: { needAuth } }, from, next) => {
  if (!needAuth) {
    return next();
  }

  if (!getAuthCookie()) {
    return next("/");
  }

  return next();
});

export default router;
