import Cookie from "universal-cookie";
import { NOTIFY_STATUS } from "@/constants/common";

const cookie = new Cookie();

export const checkNotifyStatus = status => {
  const preparedStatus = status.toLowerCase().trim();
  return NOTIFY_STATUS.includes(preparedStatus) ? preparedStatus : "success";
};

export const notifyScheme = ({ message = "text", type = "success" }) => {
  const preparedType = checkNotifyStatus(type);
  return {
    message,
    showClose: false,
    type: preparedType,
    customClass: preparedType
  };
};

export const AsideFactory = componentPath => ({
  component: import(`@/components/The/Aside/${componentPath}`)
});

export const setCookie = (jwtToken = "", expDate = 0) => {
  const expires =
    typeof expDate === "number" ? new Date(Date.now() + expDate) : expDate;
  cookie.set("jwt", jwtToken, {
    path: "/",
    expires,
    domain: "127.0.0.1",
    httpOnly: true,
    secure: false
  });
};

export const getAuthCookie = () => {
  return cookie.get("jwt");
};
