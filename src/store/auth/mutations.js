import { SET_AUTHENTICATED } from "./mutation-types";

export default {
  [SET_AUTHENTICATED](state, payload) {
    state.authenticated = payload;
  }
};
