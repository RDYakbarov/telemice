import { setCookie } from "@/utils/common";
import { registration, authorization, recoveryPassword } from "@api/auth";
import router from "@/router";
import { SET_AUTHENTICATED } from "./mutation-types";

export default {
  setAuthenticated({ commit }, payload) {
    commit(SET_AUTHENTICATED, payload);
  },
  setCookieAndRedirect({ dispatch }, { token, tokenExpDate }) {
    setCookie(token, tokenExpDate);
    dispatch("app/asideToggle", {}, { root: true });
    dispatch("setAuthenticated", true);
    router.push("/events");
  },
  async signUp({ dispatch }, params) {
    const {
      errors,
      token,
      token_expired_at: tokenExpDate
    } = await registration(params);
    if (errors) {
      return;
    }
    dispatch("setCookieAndRedirect", { token, tokenExpDate });
  },
  async signIn({ dispatch }, params) {
    const { errors, result } = await authorization(params);
    if (errors) {
      return;
    }

    const {
      token: { token, expired: tokenExpDate }
    } = result;

    dispatch("setCookieAndRedirect", { token, tokenExpDate });
  },
  async recoveryPasswordAction({ commit }, params) {
    const { errors } = await recoveryPassword(params);
    if (errors) {
      return;
    }
  }
};
