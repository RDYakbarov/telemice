import Vue from "vue";
import Vuex from "vuex";
import events from "./events";
import app from "./app";
import auth from "./auth";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  modules: {
    events,
    app,
    auth
  }
});
