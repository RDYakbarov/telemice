import { ASIDE_TOGGLE, SET_ASIDE_TYPE } from "./mutation-types";

export default {
  [ASIDE_TOGGLE](state) {
    state.asideVisible = !state.asideVisible;
  },
  [SET_ASIDE_TYPE](state, type) {
    state.currentAside = type;
  }
};
