import { ASIDE_TOGGLE, SET_ASIDE_TYPE } from "./mutation-types";

export default {
  asideToggle({ commit }) {
    commit(ASIDE_TOGGLE);
  },
  setAsideType({ commit }, type) {
    commit(SET_ASIDE_TYPE, type);
  }
};
