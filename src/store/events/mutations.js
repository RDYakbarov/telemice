import {
  GET_MY_EVENTS,
  FILTER_MY_EVENTS,
  GET_ARCHIVE_EVENTS
} from "./mutation-types";

export default {
  [GET_MY_EVENTS](state, data) {
    state.myEvents = data;
  },
  [FILTER_MY_EVENTS](state, hash) {
    state.myEvents = state.myEvents.filter(item => item !== hash);
  },
  [GET_ARCHIVE_EVENTS](state, data) {
    state.archiveEvents = data;
  }
};
