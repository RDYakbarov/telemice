import { getMyEvents, getArchiveEvents } from "@api/mocks/events";
import {
  GET_MY_EVENTS,
  FILTER_MY_EVENTS,
  GET_ARCHIVE_EVENTS
} from "./mutation-types";

export default {
  async getMyEvents({ commit }) {
    const data = await getMyEvents();
    commit(GET_MY_EVENTS, data);
  },
  async deleteEvent({ commit }, { hash, orgHash }) {
    commit(FILTER_MY_EVENTS, hash);
    return Promise.resolve();
  },
  async getArchiveEvents({ commit }) {
    const data = await getArchiveEvents();
    commit(GET_ARCHIVE_EVENTS, data);
  }
};
