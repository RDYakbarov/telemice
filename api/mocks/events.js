import { getMyEventsResponseHelper } from '../events/utils';
import { mockApi } from './util';

export const getMyEvents = () => {
    const data = {
        "status": "success",
        "message": null,
        "notification": null,
        "result": {
        "organizations": [
            {
                "title": "First & Co.",
                "hash": "82af34530",
                "type": "500",
                "events": [
                    {
                        "title": "Конференция бухгалтеров 2018 -3",
                        "date_start": "2018-10-27 09:00:00",
                        "date_end": "2019-11-30 19:00:00",
                        "icon": "/images/icon2.png",
                        "hash": "40f6b3b98f21230e85b3f5d771a5bb0a"
                    },
                    {
                        "title": "Конференция бухгалтеров 2018 -34",
                        "date_start": "2018-10-27 09:00:00",
                        "date_end": "2019-11-30 19:00:00",
                        "icon": "/images/icon2.png",
                        "hash": "40f6b3b98f21230e85b3f5d771a5bb1a"
                    }
                ]
            },
            {
                "title": "Test & Co.",
                "hash": "d80ae7c6c",
                "type": "400",
                "events": [
                    {
                        "hash": "a53e9bb1fa353d37004ca5fff6ff7b50",
                        "title": "Конференция бухгалтеров 2018 - 4",
                        "icon": "/images/icon2.png",
                        "date_start": "2018-10-31 09:00:00",
                        "date_end": "2019-12-30 19:00:00"
                    },
                    {
                        "hash": "774a94a5e9574885c0ff48a950d968d8",
                        "title": "Конгресс участников",
                        "icon": "/images/icon2.png",
                        "date_start": "2018-11-10 09:00:00",
                        "date_end": "2019-12-30 19:00:00"
                    }
                ]
            }
        ]
    }
    };
    return mockApi(getMyEventsResponseHelper(data.result.organizations));
};

export const getArchiveEvents = () => {
  const data = {
        "status": "success",
        "message": null,
        "notification": null,
        "result": [
        {
            "hash": "ae892ae637fce92e442114760ec26ce0",
            "name": "Конференция Бухгалтеров 2018",
            "updated_at": "2018-05-28 11:34:53"
        },
        {
            "hash": "4f4c522a2adabfc1182fd34de80cf1d3",
            "name": "Комикон 2018",
            "updated_at": "2018-06-03 14:12:22"
        }
    ]
    };

    return mockApi(data.result);

};