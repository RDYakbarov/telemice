export const mockApi = (cb) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(cb)
        }, 3000);
    })
};