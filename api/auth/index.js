import { registration, authorization, recoveryPassword } from './post';

export {
    registration,
    authorization,
    recoveryPassword,
}