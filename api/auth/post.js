import { http } from '@api/index';

const SIGNUP__SCHEME = {
  method: 'post',
  url: 'auth/signup',
};

const SIGNIN_SCHEME = {
  method: 'post',
  url: 'auth/',
};

const RECOVERY_SCHEME = {
  method: 'post',
  url: 'auth/reset-password',
};

export const registration = async (requestParams) => {
    return await http(SIGNUP__SCHEME, requestParams);
};

export const authorization = async (requestParams) => {
  return await http(SIGNIN_SCHEME, requestParams);
};

export const recoveryPassword = async requestParams => {
  return await http(RECOVERY_SCHEME, requestParams);
};