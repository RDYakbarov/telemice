import axios from "axios";
import projectConfig from "@/configs/project.json";
import { getAuthCookie } from "@/utils/common";

const FAILURE_STATUS = "error";

const requestInterceptor = config => {
    const authCookie = getAuthCookie();

    if(!authCookie) {
        return config;
    }

    config.headers.common['Authorization'] = `Bearer ${authCookie}`;
    return config;
};
const responseError = error => {
    return Promise.reject(error?.response?.data?.message);
};

const API = axios.create({
    baseURL: projectConfig.apiUrl
});

API.interceptors.request.use(requestInterceptor);
API.interceptors.response.use((response) => response, responseError);

export const http = async ({ method, url }, requestParams) => {
    try {

        const isGet = method.toLowerCase() === 'get';
        const request = isGet ? {
            param: requestParams,
        } : requestParams;

        const { data } = await API[method](url, request);

        if(!data || data.status === FAILURE_STATUS) {
            throw {
                status: FAILURE_STATUS,
                errors: data.message,
            };
        }
        return data;
    } catch (e) {
        const Error = new CustomEvent('API_ERROR', {
            detail: e,
        });
        window.dispatchEvent(Error);
       return e;
    }
};