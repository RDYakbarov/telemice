export const getMyEventsResponseHelper = (organizations = []) => {
    return organizations.reduce((acc, { events, hash: orgHash }) => {
        /// примешиваем orgHash к каждому евенту
        const preparedEvents = events.map(item => ({ ...item, orgHash }));
        return [...acc, ...preparedEvents];
    }, []);
};